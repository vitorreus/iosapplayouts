//
//  GasolinaAlcoolViewController.swift
//  appLayouts
//
//  Created by iossenac on 07/11/18.
//  Copyright © 2018 iossenac. All rights reserved.
//

import UIKit

class GasolinaAlcoolViewController: UIViewController {

    @IBOutlet weak var GasView: UIView! {
        didSet {
            GasView.isHidden = true
        }
    }
    @IBOutlet weak var AlcoolView: UIView! {
        didSet {
            AlcoolView.isHidden = true
        }
    }
    @IBOutlet weak var imgResultado: UIImageView!
    @IBOutlet weak var valorGasolina: UITextField!
    @IBOutlet weak var valorAlcool: UITextField!
    @IBOutlet weak var lblResultado: UILabel! {
        didSet {
            lblResultado.text = ""
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func gasolinaChanged(_ sender: Any) {
        valuesChanged()
    }
    @IBAction func alcoolChanged(_ sender: Any) {
        valuesChanged()
    }
    
    func valuesChanged(){
        let gasolina =  Double(valorGasolina.text!)
        let alcool = Double(valorAlcool.text!)
        if (gasolina == nil || gasolina! <= 0.0 || alcool == nil || alcool! <= 0.0){
            GasView.isHidden = true
            AlcoolView.isHidden = true
            lblResultado.text = "Erro"
            return
        }
        let razao =  alcool! / gasolina!
        
        if (razao > 0.7){
            GasView.isHidden = false
           // AlcoolView.isHidden = true
            lblResultado.text = "Gasolina"
            imgResultado.image = UIImage(named:"if_gas_1055052.png")!
        } else {
            //GasView.isHidden = true
            //AlcoolView.isHidden = false
            GasView.isHidden = false
            lblResultado.text = "Álcool"
            imgResultado.image = UIImage(named:"if_leaf-spring-plant-ecology-green_2189582.png")!
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
